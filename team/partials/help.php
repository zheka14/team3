<?php 
	require($_SERVER['DOCUMENT_ROOT'] . "/partials/menu.php");
?>
<div style="color: #fff;
    margin-top: 5%;
    margin-left: 20%;
    margin-right: 20%;">
	<h1>Допомога в користуванні </h1>
	<h3>Ви знаходитеся на даній сторінці,отже реєстрація/вхід пройшли вдало
	і вам стане в нагоді даний гайд </h3>
	<h3>Даний сайт(застосунок)створювався для полегшення роботи нашим волонтерам</h3>
	<h2>
		ГОЛОВНА ТАБЛИЦЯ:
	</h2>
	<h3>
		В даній таблиці Ви можете спостерігати наяні у вашому "рюкзаку" предмети за різними категоріями,які Ви поспосередньо самі добавляєте.Перейшовши за будь-якою з категорій,Вам відкриється таблиця предметів,які можна видалити після того,як ви віддали необхідне "замовнику".
	</h3>
	<h2>
		ГОЛОВНЕ МЕНЮ
	</h2>
	<h3>
		З самого початку Ви знаходилися в головному вікні,в якому Вам відкрилася можливість перейти за такими посиланнями:
	</h3>

	<h2>
		ДОДАТИ:
	</h2>
	<h3>
		В даному пункті для Вас відкриються лінки за категоріями,в яких можна додати предмети як в "рюкзак",так і в "потреби" натиснувши на відповідну кнопку.  
	</h3>
	<h2>
		ПОТРЕБИ:
	</h2>
	<h3>
		Також відкриває лінки за категоріями.Дане меню створене для швидкого знаходження предметів,які саме Вам потрібно знайти.
		Для зручності в даних таблицях присутня кнопка видалення запису,коли Ви його вже "відправили" в "рюкзак".
	</h3>
	<h2>
		ГОЛОВНА:
	</h2>
	<h3>
		Відправляє Вас до головного вікна.
	</h3>
	
	<h2>
		ВИХІД:
	</h2>
	<h3>
		Ви вийдете зі свого акаунту.
	</h3>

	<h2>
		КОРОТКО,ПРО РОБОЧУ ЛОГІКУ ТУТ:
	</h2>
	<h3>
		Зайшли-перейшли в "ДОДАТИ"-додали необхідне в "ПОТРЕБИ"-зайшли в "ПОТРЕБИ" подивитися на необхідне-коли знайшли необхідне,перейшли в "ДОДАТИ"-натиснули додати в "РЮКЗАК"-зайшли в "Потреби"-видалили знайдений предмет-після передачі необхідного з "РЮКЗАКА" видаляєте запис і звідти. 
	</h3>

</div>	