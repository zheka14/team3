 $('body').css({'background': "#32CD32"});

//прописуємо код для видалення елементів зі списку "ПОТРЕБИ" 
$('.needMedDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/need_del/need_med_del.php?user_id=' + ID;
})


$('.needMilitDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/need_del/need_milit_del.php?user_id=' + ID;
})


$('.needOtherDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/need_del/need_other_del.php?user_id=' + ID;
})

$('.needProdDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/need_del/need_prod_del.php?user_id=' + ID;
})

//=====================================================

//прописуємо код для видалення елементів зі списку "РЮКЗАК" 

$('.avMedDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/bag_del/bag_med_del.php?user_id=' + ID;
})

$('.avMilitDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/bag_del/bag_milit_del.php?user_id=' + ID;
})

$('.avOtherDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/bag_del/bag_other_del.php?user_id=' + ID;
})

$('.avProdDel').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.id;
	console.dir(ID);

	location='/bag_del/bag_prod_del.php?user_id=' + ID;
})
//====================================================
//прописуємо код для додавання елементів в список "ПОТРЕБИ" 
$('.needMedAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.med;
	console.dir(ID);

	location='../need-add/need_med_add.php?user_id=' + ID;
})

$('.needMilitAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.milit;
	console.dir(ID);

	location='../need-add/need_milit_add.php?user_id=' + ID;
})

$('.needOtherAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.other;
	console.dir(ID);

	location='../need-add/need_other_add.php?user_id=' + ID;
})

$('.needProdAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.prod;
	console.dir(ID);

	location='../need-add/need_prod_add.php?user_id=' + ID;
})

//прописуємо код для додавання елементів в список "Рюкзак" 
$('.bagMedAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.med;
	console.dir(ID);

	location='../bag-add/bag_med_add.php?user_id=' + ID;
})

$('.bagMilitAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.milit;
	console.dir(ID);

	location='../bag-add/bag_milit_add.php?user_id=' + ID;
})

$('.bagOtherAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.other;
	console.dir(ID);

	location='../bag-add/bag_other_add.php?user_id=' + ID;
})

$('.bagProdAdd').on('click', (e) => {
	let element = e.target;

	let ID = element.dataset.prod;
	console.dir(ID);

	location='../bag-add/bag_prod_add.php?user_id=' + ID;
})

